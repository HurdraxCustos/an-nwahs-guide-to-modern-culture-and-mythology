# An N'wah's Guide To Modern Culture And Mythology

A companion book for the [Expanded Vanilla](https://modding-openmw.com/lists/expanded-vanilla/) and [Total Overhaul](https://modding-openmw.com/lists/total-overhaul/) mod lists of [Modding-OpenMW.com](https://modding-openmw.com/)

An odd stranger greets you in Seyda Neen and gives you a "guide book" as a gift.

The process of going through the mod lists on Modding-OpenMW.com is a long one, and by the time you've finished you may have lost track of all the new content you now have to see. That's where "The N'Wah's Guide" comes in.

I created this mod with the goal of giving players an in-game guide to the new content they added via the mod lists.

This mod is potentially immersion-breaking and not lore friendly.

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* Benjamin Winger (for Delta Plugin)
* eddie5 (scripting help)
* EvilEye (for making Cs.js)
* gonzo (testing and feedback)
* Greateness7 (for making tes3conv)
* Ignacious (scripting help)
* PoodleSandwich (scripting help)
* Settyness (audio and scripting help)
* The OpenMW team, including every contributor (for making OpenMW)

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Mod Home](https://modding-openmw.gitlab.io/an-nwahs-guide-to-modern-culture-and-mythology/)

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### Installation

1. Download the zip from a link above (OpenMW 0.48.0 or newer is required!) 
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Books\NwahsGuide

        # Linux
        /home/username/games/OpenMWMods/Books/NwahsGuide

        # macOS
        /Users/username/games/OpenMWMods/Books/NwahsGuide
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\Books\NwahsGuide"
1. Enable the mod plugin via OpenMW-Launcher ([video](https://www.youtube.com/watch?v=xzq_ksVuRgc)), or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=an-nwahs-guide-to-modern-culture-and-mythology.omwaddon
1. An NPC named `johnnyhostile` will be waiting for you outside of the Census and Excise offices after starting a new game, he is standing by the wall. Talk to him to receive the guide. He will leave after one week.

#### Give Feedback

Did you find an error, or did you want to add or remove an excerpt? Please [open an issue](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/issues/new)!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods]()
