#!/bin/sh
set -e

if [ -n "$CI" ]; then
    mkdir -p "$HOME"/.config/openmw
    echo "data=\"/srv\"" > "$HOME"/.config/openmw/openmw.cfg
fi

delta_plugin convert an-nwahs-guide-to-modern-culture-and-mythology.yaml
