#!/bin/sh
set -eu

file_name=an-nwahs-guide-to-modern-culture-and-mythology.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

mkdir images
cp web/site/img/an-nwahs-guide-to-modern-culture-and-mythology*.png images/

zip --must-match --recurse-paths $file_name \
    an-nwahs-guide-to-modern-culture-and-mythology.omwaddon \
    CHANGELOG.md \
    images \
    LICENSE \
    README.md \
    sound \
    version.txt
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
rm -rf images
